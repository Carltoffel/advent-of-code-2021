program day10
use iso_fortran_env, only: int64
use parser_input_m, only: parser_input_t
implicit none

character(*), parameter :: FILENAME = "input/day10"

type(parser_input_t), allocatable :: parser_input(:)

allocate(parser_input(0))
parser_input = parser_input_t( read_navigation_subsystem(FILENAME) )
call parse_chunks(parser_input)
write(*,"('Error Score of corrupt lines   : 'i0)") &
        sum(parser_input%syntax_error_score, &
            mask=.not.parser_input%end_of_input)
write(*,"('Error Score of completed lines : 'i0)") &
        get_median( &
                   pack(parser_input%syntax_error_score, &
                        mask=parser_input%end_of_input))

contains



function get_median(list) result(median)
integer(int64), intent(in) :: list(:)
integer(int64) :: median

integer(int64) :: sorted_list(size(list))

sorted_list = qsort(list)
median = sorted_list(size(list)/2+1)
end function get_median



recursive function qsort(list) result(sorted_list)
integer(int64), intent(in) :: list(:)
integer(int64) :: sorted_list(size(list))

if(size(list)>1) then
    sorted_list = [qsort(pack(list, list>list(1))), &
                   list(1),                         &
                   qsort(pack(list, list<list(1)))]
else
    sorted_list = list
end if
end function



function read_navigation_subsystem(filename) result(syntax)
use parser_m, only: get_number_of_lines
character(*), intent(in) :: filename
character(128), allocatable :: syntax(:)

integer :: fileunit
integer :: n_lines

n_lines = get_number_of_lines(filename)
allocate(syntax(n_lines))
open(newunit=fileunit, file=filename)
read(fileunit,*) syntax
close(fileunit)
end function read_navigation_subsystem



impure elemental recursive subroutine parse_chunks(input)
type(parser_input_t), intent(inout) :: input

character(1) :: closing_char

do while(.not.input%is_end_of_input())
    closing_char = get_closing_char(input%peek())
    if(closing_char == achar(0)) exit

    input%pos = input%pos + 1
    call parse_chunks(input)

    if(input%end_of_input) then
        call input%update_score( get_autocorrect_score(closing_char) )
        exit
    else if(input%syntax_error_score == 0) then
        if(input%peek() /= closing_char) then
            input%syntax_error_score = &
                    get_corrupt_score( input%peek() )
        end if
    else
        exit
    end if
    input%pos = input%pos + 1
end do
end subroutine parse_chunks



elemental function get_closing_char(opening_char) result(closing_char)
character(1), intent(in) :: opening_char
character(1) :: closing_char

character(4), parameter :: opening_chars = "([{<"
character(4), parameter :: closing_chars = ")]}>"

associate(char_idx => index(opening_chars, opening_char))
    if(char_idx > 0) then
        closing_char = closing_chars(char_idx:char_idx)
    else
        closing_char = achar(0)
    end if
end associate
end function get_closing_char



elemental function get_corrupt_score(wrong_char) result(score)
character(1), intent(in) :: wrong_char
integer :: score

integer,      parameter :: scores(*)     = [3, 57, 1197, 25137]
character(4), parameter :: closing_chars = ")]}>"

score = scores( index(closing_chars, wrong_char) )
end function get_corrupt_score



elemental function get_autocorrect_score(missing_char) result(score)
character(1), intent(in) :: missing_char
integer :: score

character(4), parameter :: closing_chars = ")]}>"

score = index(closing_chars, missing_char)
end function get_autocorrect_score



end program
