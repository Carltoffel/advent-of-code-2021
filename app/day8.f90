program day8
implicit none

character(*), parameter :: FILENAME = "input/day8"

character(7), allocatable :: segments(:,:)

allocate(segments(0,0))
segments = read_digit_segments(FILENAME)
write(*,"('Number of 1s, 4s, 7s and 8s : 'i7)") &
        count_unambiguous_numbers(segments)
write(*,"('Sum of all numbers          : 'i7)") &
        sum_numbers(segments)

contains



function read_digit_segments(filename) result(segments)
use parser_m, only: get_number_of_lines
character(*), intent(in) :: filename
character(7), allocatable :: segments(:,:)

integer :: fileunit
integer :: n_lines

n_lines = get_number_of_lines(filename)
allocate(segments(15, n_lines))
open(newunit=fileunit, file=filename)
read(fileunit, *) segments
close(fileunit)
end function



integer function count_unambiguous_numbers(segments) result(counter)
character(7), intent(in) :: segments(:,:)

integer :: i_entry
integer :: i_digit
integer :: n_entries

associate(segments_shape => shape(segments))
    n_entries = segments_shape(2)
end associate
do i_entry=1, n_entries
    do i_digit=12, 15
        if(any([2, 3, 4, 7] == len(trim(segments(i_digit, i_entry))))) &
            counter = counter + 1
    end do
end do
end function



integer function sum_numbers(segments)
character(7), intent(in) :: segments(:,:)

integer :: n_entries
integer :: i

sum_numbers = 0
associate(segments_shape => shape(segments))
    n_entries = segments_shape(2)
end associate
do i=1,n_entries
    sum_numbers = sum_numbers + decode_number(segments(:,i))
end do
end function



function decode_number(line) result(decoded_number)
character(7), intent(in) :: line(15)
integer :: decoded_number

integer, parameter :: basis(4) = [1000, 100, 10, 1]
character(1) :: seg2
character(1) :: seg3
character(1) :: seg5
integer      :: decoded_digits(4)

seg2 = identify_segment2(line(:10))
seg3 = identify_segment3(line(:10))
seg5 = identify_segment5(line(:10))
decoded_digits = decode_digit(line(12:15), seg2, seg3, seg5)
decoded_number = sum(decoded_digits * basis)
end function



function identify_segment2(unique_digits) result(seg2)
character(7), intent(in) :: unique_digits(10)
character(1) :: seg2

character, parameter :: chars(7) = ['a', 'b', 'c', 'd', 'e', 'f', 'g']
integer :: i

do i=1,7
    if(count_chars(unique_digits, chars(i)) == 6) then
        seg2 = chars(i)
        exit
    end if
end do
end function



function identify_segment5(unique_digits) result(seg5)
character(7), intent(in) :: unique_digits(10)
character(1) :: seg5

character, parameter :: chars(7) = ['a', 'b', 'c', 'd', 'e', 'f', 'g']
integer :: i

do i=1,7
    if(count_chars(unique_digits, chars(i)) == 4) then
        seg5 = chars(i)
        exit
    end if
end do
end function



function identify_segment3(unique_digits) result(seg3)
character(7), intent(in) :: unique_digits(10)
character(1) :: seg3

character, parameter :: chars(7) = ['a', 'b', 'c', 'd', 'e', 'f', 'g']
integer :: i
character(2) :: one

do i=1,10
    if(len(trim(unique_digits(i))) == 2) one = trim(unique_digits(i))
end do

do i=1,7
    if(count_chars(unique_digits, chars(i)) == 8) then
        if(chars(i) == one(1:1) .or. chars(i) == one(2:2)) then
            seg3 = chars(i)
            exit
        end if
    end if
end do
end function



integer function count_chars(unique_digits, c) result(counter)
character(7), intent(in) :: unique_digits(10)
character(1), intent(in) :: c

integer :: digit
integer :: segment

counter = 0
do digit=1,10
    do segment=1,7
        if(unique_digits(digit)(segment:segment) == c) counter = counter+1
    end do
end do
end function



elemental function decode_digit(digit_segments, seg2, seg3, seg5) result(digit)
character(7), intent(in) :: digit_segments
character(1), intent(in) :: seg2
character(1), intent(in) :: seg3
character(1), intent(in) :: seg5
integer :: digit

select case(len(trim(digit_segments)))
case(2)
    digit = 1
case(3)
    digit = 7
case(4)
    digit = 4
case(5)
    if(index(digit_segments, seg5)>0) then
        digit = 2
    else if(index(digit_segments, seg2)>0) then
        digit = 5
    else
        digit = 3
    end if
case(6)
    if(index(digit_segments, seg5)==0) then
        digit = 9
    else if(index(digit_segments, seg3)==0) then
        digit = 6
    else
        digit = 0
    end if
case(7)
    digit = 8
end select
end function



end program day8
