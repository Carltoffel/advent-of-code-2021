program day1
use parser_m, only: integer_parser
implicit none

character(*), parameter :: filename = "input/day1"

associate(measurements => integer_parser(filename))
    write(*,"('Number of increases            : 'g0)") &
            count_increases(measurements)
    write(*,"('Number of increases (smoothed) : 'g0)") &
            count_increases(smooth(measurements))
end associate

contains



function count_increases(array) result(increases)
integer, intent(in) :: array(:)
integer :: increases

associate( differences => (array(2:) - array(:size(array)-1)) )
    increases = count(differences > 0)
end associate
end function count_increases



function smooth(array) result(smoothed)
integer, intent(in) :: array(:)
integer :: smoothed(size(array)-2)

associate( length => size(array) )
smoothed = array( :length-2) + &
           array(2:length-1) + &
           array(3:        )
end associate
end function



end program day1
