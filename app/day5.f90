program day5
use line_m, only: line_t
implicit none

character(*), parameter :: FILENAME = "input/day5"

type(line_t), allocatable :: lines(:)
integer,      allocatable :: field(:,:)
integer                   :: i

allocate(lines(0))
lines = read_lines(FILENAME)
associate(fieldsize => maxval(lines%maximum()))
    allocate(field(fieldsize+1, fieldsize+1))
end associate
field = 0
do i=1, size(lines)
    call add_line(field, lines(i))
end do
write(*,"('Overlaps: ',g0)") count(field > 1)

contains



function read_lines(filename) result(lines)
use parser_m, only: get_number_of_lines
character(*), intent(in) :: filename
type(line_t), allocatable :: lines(:)

integer      :: fileunit
integer      :: i
character(3) :: tmp

associate(n_lines => get_number_of_lines(filename))
    allocate(lines(n_lines))
    open(newunit=fileunit, file=filename)
    do i=1,n_lines
        read(fileunit, *) lines(i)%from, tmp, lines(i)%till
    end do
    close(fileunit)
end associate
end function read_lines



subroutine add_line(field, line)
integer,      intent(inout) :: field(:,:)
type(line_t), intent(in)    :: line

integer :: i
integer :: x
integer :: y

if(line%is_diagonal().and.line%is_45_degrees()) then
    do i=0, abs(line%till(1) - line%from(1))
        x = line%from(1) + sign(i, (line%till(1) - line%from(1))) +1
        y = line%from(2) + sign(i, (line%till(2) - line%from(2))) +1
        call incr(field(x,y))
    end do
else
    associate( &
            x1 => min(line%from(1), line%till(1)) +1, &
            x2 => max(line%from(1), line%till(1)) +1, &
            y1 => min(line%from(2), line%till(2)) +1, &
            y2 => max(line%from(2), line%till(2)) +1)
        call incr(field(x1:x2, y1:y2))
    end associate
end if
end subroutine



elemental subroutine incr(x)
integer, intent(inout) :: x
x = x + 1
end subroutine incr



end program day5
