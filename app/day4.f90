program day4
implicit none

type bingo_board_t
    integer :: numbers(5, 5)
    logical :: crossed(5, 5) = .false.
end type bingo_board_t

character(*), parameter :: FILENAME = "input/day4"
type(bingo_board_t), allocatable :: bingo_boards(:)
logical, allocatable             :: board_won(:)
integer                          :: i

associate(picked_numbers => read_picked_numbers(FILENAME))
    bingo_boards = read_bingo_boards(FILENAME)
    do i=1, size(picked_numbers)
        call cross_number(bingo_boards, picked_numbers(i))
        board_won = check_if_board_has_won(bingo_boards)
        if(any(check_if_board_has_won(bingo_boards))) then
            associate(winner_board_index => findloc(board_won, .true., 1))
                write(*, "('Winner Score = 'i0)") picked_numbers(i) * &
                        calc_score(bingo_boards(winner_board_index))
            end associate
            where(board_won)
                bingo_boards = bingo_board_t(0, .false.)
            end where
        end if
    end do
end associate
contains



function read_picked_numbers(filename) result(numbers)
character(*), intent(in) :: filename
integer, allocatable :: numbers(:)

integer         :: fileunit
character(1000) :: first_line

open(newunit=fileunit, file=filename)
read(fileunit, "(a)") first_line
close(fileunit)
allocate(numbers(count_commas(first_line)))
read(first_line, *) numbers
end function



function count_commas(str) result(n_commas)
character(*), intent(in) :: str
integer :: n_commas

integer :: i

n_commas = 0
do i=1, len(str)
    if(str(i:i) == ',') n_commas = n_commas + 1
end do
end function count_commas



function read_bingo_boards(filename) result(bingo_boards)
use parser_m, only: get_number_of_lines
character(*), intent(in) :: filename
type(bingo_board_t), allocatable :: bingo_boards(:)

integer :: i
integer :: fileunit

associate(n_boards => (get_number_of_lines(filename)-1)/6)
    allocate(bingo_boards(n_boards))
    open(newunit=fileunit, file=filename)
    read(fileunit, *)
    do i=1,n_boards
        read(fileunit, *) bingo_boards(i)%numbers
    end do
end associate
close(fileunit)
end function



elemental subroutine cross_number(bingo_board, picked_number)
type(bingo_board_t), intent(inout) :: bingo_board
integer,             intent(in)    :: picked_number

where(bingo_board%numbers == picked_number)
    bingo_board%crossed = .true.
end where
end subroutine cross_number



elemental function check_if_board_has_won(bingo_board) result(won)
type(bingo_board_t), intent(in) :: bingo_board
logical :: won

won = any(count(bingo_board%crossed, 1) == 5) .or. &
      any(count(bingo_board%crossed, 2) == 5)
end function



elemental function calc_score(bingo_board) result(score)
type(bingo_board_t), intent(in) :: bingo_board
integer :: score

score = sum(bingo_board%numbers, .not.bingo_board%crossed)
end function



end program day4
