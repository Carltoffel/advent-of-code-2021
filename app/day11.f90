program day9
use iso_fortran_env, only: int8
implicit none

character(*), parameter :: FILENAME = "input/day11"

integer(int8),  allocatable :: padded_octopuses(:,:)
integer :: height
integer :: width
integer :: i, j
integer :: sum_of_flashes

sum_of_flashes = 0
allocate(padded_octopuses(0,0))
padded_octopuses = read_octopuses(FILENAME)
associate(map_shape => shape(padded_octopuses))
    height = map_shape(2)
    width  = map_shape(1)
end associate
associate(octopuses  => padded_octopuses(2:width-1, 2:height-1), &
          east       => padded_octopuses(3:width  , 2:height-1), &
          north_east => padded_octopuses(3:width  , 1:height-2), &
          north      => padded_octopuses(2:width-1, 1:height-2), &
          north_west => padded_octopuses(1:width-2, 1:height-2), &
          west       => padded_octopuses(1:width-2, 2:height-1), &
          south_west => padded_octopuses(1:width-2, 3:height  ), &
          south      => padded_octopuses(2:width-1, 3:height  ), &
          south_east => padded_octopuses(3:width  , 3:height  ))
    do i=1,1000
        octopuses = octopuses + 1_int8
        do while(count(octopuses > 9) > 0)
            sum_of_flashes = sum_of_flashes + count(octopuses > 9)
            where(octopuses > 9)
                octopuses  = -127_int8
                east       =       east + 1_int8
                north_east = north_east + 1_int8
                north      = north      + 1_int8
                north_west = north_west + 1_int8
                west       = west       + 1_int8
                south_west = south_west + 1_int8
                south      = south      + 1_int8
                south_east = south_east + 1_int8
            end where
            padded_octopuses( :       ,[1,height]) = 0
            padded_octopuses([1,width], :        ) = 0
        end do
        octopuses = max(octopuses, 0)
        print"(10i1)",octopuses
        print*, sum_of_flashes
        if(count(octopuses == 0) == 100) exit
    end do
    print*, i
end associate

contains



function read_octopuses(filename) result(octopuses)
use parser_m, only: get_number_of_lines
character(*), intent(in) :: filename
integer(int8), allocatable :: octopuses(:,:)

integer         :: fileunit
integer         :: n_lines
integer         :: n_columns
character(1000) :: tmp_line

n_lines = get_number_of_lines(filename)
open(newunit=fileunit, file=filename)
read(fileunit, "(a)") tmp_line
n_columns = len(trim(tmp_line))
allocate(octopuses( 1+n_columns+1, 1+n_lines+1 ))
octopuses( :             ,[1,n_lines+2]) = 0
octopuses([1,n_columns+2], :           ) = 0
read(tmp_line,"(10i1)") octopuses(2:n_columns+1, 2)
read(fileunit,"(10i1)") octopuses(2:n_columns+1, 3:n_lines+1)
close(fileunit)
end function read_octopuses



end program day9
