program day3
use parser_m, only: get_number_of_lines
implicit none

integer,      parameter :: NBITS = 12
character(*), parameter :: filename = "input/day3"
integer                 :: i
integer,      parameter :: basis(NBITS) = [(2**i, i=NBITS-1, 0, -1)]

integer :: gamma_rate
integer :: epsilon_rate
integer :: o2_generator_rating
integer :: co2_scrubber_rating

associate(report => read_report(filename))
    associate(most_common_bits => most_common_bits(report))
        gamma_rate   = sum(basis,      most_common_bits)
        epsilon_rate = sum(basis, .not.most_common_bits)
    end associate
    o2_generator_rating = &
            sum(basis * o2_generator_rating_binary(report))
    co2_scrubber_rating = &
            sum(basis * co2_scrubber_rating_binary(report))
end associate
write(*,"('Gamma Rate          : 'g0)") gamma_rate
write(*,"('Epsilon Rate        : 'g0)") epsilon_rate
write(*,"('Power Consumption   : 'g0)") gamma_rate * &
                                        epsilon_rate
write(*,"('O2-Generator-Rating : 'g0)") o2_generator_rating
write(*,"('CO2-Scrubber-Rating : 'g0)") co2_scrubber_rating
write(*,"('Life-Support-Rating : 'g0)") o2_generator_rating * &
                                            co2_scrubber_rating

contains



pure function most_common_bits(report)
integer, intent(in) :: report(:, :)
logical :: most_common_bits(NBITS)

most_common_bits = 2*sum(report, 2) > size(report)/NBITS
end function



function read_report(filename) result(report)
character(*), intent(in) :: filename
integer, allocatable :: report(:, :)

integer      :: fileunit
integer      :: linenumber

associate(number_of_lines => get_number_of_lines(filename))
    allocate( report(NBITS, number_of_lines) )
    open(newunit=fileunit, file=filename)
    do linenumber=1, number_of_lines
        read(fileunit, "(12i1)") report(:, linenumber)
    end do
end associate
end function read_report



function o2_generator_rating_binary(array)
integer, intent(in) :: array(:, :)
integer :: o2_generator_rating_binary(NBITS)

logical :: mask(size(array)/NBITS)

mask = .true.
do i=1,NBITS
associate(bit_column => array(i, :))
    where(mask)
        mask = bit_column == get_most_common_bit(bit_column, mask)
    end where
end associate
end do
o2_generator_rating_binary = array(:, findloc(mask, .true., 1))
end function



function get_most_common_bit(column, mask) result(bit)
integer, intent(in) :: column(:)
logical, intent(in) :: mask(size(column))
integer :: bit

if(2*sum(column, mask) >= count(mask)) then
    bit = 1
else
    bit = 0
end if
end function get_most_common_bit



function co2_scrubber_rating_binary(array)
integer, intent(in) :: array(:, :)
integer :: co2_scrubber_rating_binary(NBITS)

logical :: mask(size(array)/NBITS)

mask = .true.
do i=1,NBITS
    associate(bit_column => array(i, :))
        where(mask)
            mask = array(i, :) == least_common_bit(array(i, :), mask)
        end where
    end associate
end do
co2_scrubber_rating_binary = array(:, findloc(mask, .true., 1))
end function co2_scrubber_rating_binary



function least_common_bit(column, mask) result(bit)
integer, intent(in) :: column(:)
logical, intent(in) :: mask(size(column))
integer :: bit

associate(n_ones => sum(column, mask), n_numbers => count(mask))
if(n_ones == 0) then
    bit = 0
else if(n_ones == n_numbers) then
    bit = 1
else if(2*n_ones < n_numbers) then
    bit = 1
else
    bit = 0
end if
end associate
end function least_common_bit



end program day3
