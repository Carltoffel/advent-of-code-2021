program day9
use iso_fortran_env, only: int8, int16
implicit none

character(*), parameter :: FILENAME = "input/day9"

integer(int8),  allocatable :: padded_height_map(:,:)
integer(int16), allocatable :: smoke_units(:,:)
integer :: height
integer :: width
integer :: i

allocate(padded_height_map(0,0))
padded_height_map = read_height_map(FILENAME)
associate(map_shape => shape(padded_height_map))
    height = map_shape(2)
    width  = map_shape(1)
end associate
allocate(smoke_units(width, height))
associate(height_map => padded_height_map(2:width-1, 2:height-1), &
          west       => padded_height_map(1:width-2, 2:height-1), &
          east       => padded_height_map(3:width  , 2:height-1), &
          north      => padded_height_map(2:width-1, 1:height-2), &
          south      => padded_height_map(2:width-1, 3:height  ))
    print*,sum(height_map+1, mask=height_map < min(west, east, north, south))
end associate
where(padded_height_map == 9)
    smoke_units = 0
else where
    smoke_units = 1
end where
do i=1,8
    call simulate_flow(smoke_units, padded_height_map)
    !print"(102i1)", smoke_units
    !print*
end do
print*, pack(smoke_units, smoke_units>100)

contains



subroutine simulate_flow(smoke_units, height_map)
integer(int16), intent(inout) :: smoke_units(:,:)
integer(int8),  intent(in)    :: height_map(:,:)

integer :: l
integer :: c
integer :: width
integer :: height

associate(map_shape => shape(height_map))
    height = map_shape(2)
    width  = map_shape(1)
end associate
do l=2,height-1
    do c=2,width-1
        if(height_map(c,l) > height_map(c,l-1)) then
            smoke_units(c,l-1) = smoke_units(c,l-1) + smoke_units(c,l)
            smoke_units(c,l) = 0
        else if(height_map(c,l) > height_map(c,l+1)) then
            smoke_units(c,l+1) = smoke_units(c,l+1) + smoke_units(c,l)
            smoke_units(c,l) = 0
        else if(height_map(c,l) > height_map(c-1,l)) then
            smoke_units(c-1,l) = smoke_units(c-1,l) + smoke_units(c,l)
            smoke_units(c,l) = 0
        else if(height_map(c,l) > height_map(c+1,l)) then
            smoke_units(c+1,l) = smoke_units(c+1,l) + smoke_units(c,l)
            smoke_units(c,l) = 0
        end if
    end do
end do
end subroutine



function read_height_map(filename) result(height_map)
use parser_m, only: get_number_of_lines
character(*), intent(in) :: filename
integer(int8), allocatable :: height_map(:,:)

integer         :: fileunit
integer         :: n_lines
integer         :: n_columns
character(1000) :: tmp_line

n_lines = get_number_of_lines(filename)
open(newunit=fileunit, file=filename)
read(fileunit, "(a)") tmp_line
n_columns = len(trim(tmp_line))
allocate(height_map( 1+n_columns+1, 1+n_lines+1 ))
height_map( :             ,[1,n_lines+2]) = 9
height_map([1,n_columns+2], :           ) = 9
read(tmp_line,"(100i1)") height_map(2:n_columns+1, 2)
read(fileunit,"(100i1)") height_map(2:n_columns+1, 3:n_lines+1)
close(fileunit)
end function read_height_map



end program day9
