program day7
implicit none

character(*), parameter :: FILENAME = "input/day7"

integer, allocatable :: crab_positions(:)

crab_positions = read_crab_positions(FILENAME)
associate(mean => golden_ratio_search(crab_positions, 1, 1000))
    print*,mean
    print*,fuel_cost(crab_positions, mean)
end associate

contains



function golden_ratio_search(crab_positions, lower_limit, upper_limit) result(mean)
integer, intent(in) :: crab_positions(:)
integer, intent(in) :: lower_limit
integer, intent(in) :: upper_limit
integer :: mean

real, parameter :: golden_ratio = (sqrt(5.)+1) / 2
real :: a, b, c, d

a = lower_limit
b = upper_limit
c = b - (b-a) / golden_ratio
d = a + (b-a) / golden_ratio
do while ( abs(a-b) > 1 )
    print*, a, b
    if(fuel_cost(crab_positions, nint(c)) < fuel_cost(crab_positions, nint(d))) then
        b = d
    else
        a = c
    end if
    c = b - (b-a) / golden_ratio
    d = a + (b-a) / golden_ratio
end do
mean = nint(a)
end function



integer function fuel_cost(crab_positions, mean)
integer, intent(in) :: crab_positions(:)
integer, intent(in) :: mean
!fuel_cost = sum(abs(crab_positions - mean))
fuel_cost = sum(abs(crab_positions - mean)*(abs(crab_positions-mean)+1)/2)
end function fuel_cost



function read_crab_positions(filename) result(positions)
character(*), intent(in) :: filename
integer, allocatable :: positions(:)

integer         :: fileunit
character(10000) :: first_line

open(newunit=fileunit, file=filename)
read(fileunit, "(a)") first_line
close(fileunit)
allocate(positions(count_commas(first_line)+1))
read(first_line, *) positions
end function read_crab_positions



function count_commas(str) result(n_commas)
character(*), intent(in) :: str
integer :: n_commas

integer :: i

n_commas = 0
do i=1, len(str)
    if(str(i:i) == ',') n_commas = n_commas + 1
end do
end function count_commas



end program day7
