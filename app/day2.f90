program day2
implicit none

character(*), parameter :: filename = "input/day2"

integer :: position(3)

position(:2) = track_position(filename)
write(*,"('Horizontal Position : 'g0)") position(1)
write(*,"('Depth               : 'g0)") position(2)
write(*,"('Position Product    : 'g0)") product(position(:2))
position = track_position_with_aim(filename)
write(*,"('Horizontal Position : 'g0)") position(1)
write(*,"('Depth               : 'g0)") position(2)
write(*,"('Aim                 : 'g0)") position(3)
write(*,"('Position Product    : 'g0)") product(position(:2))

contains



function track_position(filename) result(position)
character(*), intent(in) :: filename
integer :: position(2)

integer      :: file_unit
integer      :: ierr
character(9) :: line

position = 0
open(newunit=file_unit, file=filename)
ierr = 0
line = ""
do while(ierr == 0)
    position = position + get_direction(line)
    read(file_unit, "(a)", iostat=ierr) line
end do
close(file_unit)
end function track_position



function track_position_with_aim(filename) result(position)
character(*), intent(in) :: filename
integer :: position(3)

integer      :: file_unit
integer      :: ierr
character(9) :: line

position = 0
open(newunit=file_unit, file=filename)
ierr = 0
line = ""
do while(ierr == 0)
    call update_position(position, line)
    read(file_unit, "(a)", iostat=ierr) line
end do
close(file_unit)
end function track_position_with_aim



function get_direction(command) result(direction)
character(9), intent(in) :: command
integer :: direction(2)

integer :: distance

select case(command(1:1))
case('f')
    distance  = ichar(command(9:9)) - ichar('0')
    direction = [distance, 0]
case('u')
    distance  = ichar(command(4:4)) - ichar('0')
    direction = [0, -distance]
case('d')
    distance  = ichar(command(6:6)) - ichar('0')
    direction = [0, distance]
case default
    direction = [0, 0]
end select
end function get_direction



subroutine update_position(position, command)
integer,      intent(inout) :: position(3)
character(9), intent(in)    :: command

integer :: amount

select case(command(1:1))
case('f')
    amount      = ichar(command(9:9)) - ichar('0')
    position(1) = position(1) + amount
    position(2) = position(2) + amount * position(3)
case('u')
    amount      = ichar(command(4:4)) - ichar('0')
    position(3) = position(3) - amount
case('d')
    amount      = ichar(command(6:6)) - ichar('0')
    position(3) = position(3) + amount
end select
end subroutine update_position



end program day2
