program day6
use iso_fortran_env, only: int64
implicit none

character(*), parameter :: FILENAME            = "input/day6"
integer,      parameter :: N_SIMULATION_DAYS   = 256

integer(int64) :: timers(9)
integer        :: day

timers = read_initial_timers(FILENAME)
do day=1, N_SIMULATION_DAYS
    timers = [timers(2:7), timers(8)+timers(1), timers(9), timers(1)]
end do
print*, sum(timers)
contains



elemental subroutine decrease(x)
integer(int64), intent(inout) :: x
x = x - 1
end subroutine decrease



function read_initial_timers(filename) result(timers)
character(*), intent(in) :: filename
integer :: timers(0:8)

integer, allocatable :: numbers(:)
integer              :: fileunit
character(1000)      :: first_line
integer              :: internal_timer_value

open(newunit=fileunit, file=filename)
read(fileunit, "(a)") first_line
close(fileunit)
allocate(numbers( (len(trim(first_line))+1)/2 ))
read(first_line, *) numbers
do internal_timer_value=0,8
    timers(internal_timer_value) = &
            count(numbers == internal_timer_value)
end do
end function read_initial_timers



end program day6
