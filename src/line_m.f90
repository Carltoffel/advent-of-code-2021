module line_m
implicit none

type line_t
    integer :: from(2)
    integer :: till(2)
contains
    procedure :: is_diagonal
    procedure :: is_45_degrees
    procedure :: maximum
end type line_t

contains



elemental logical function is_diagonal(self)
class(line_t), intent(in) :: self
is_diagonal = all(self%from /= self%till)
end function is_diagonal



elemental logical function is_45_degrees(self)
class(line_t), intent(in) :: self

associate(distance => abs(self%till - self%from))
    is_45_degrees = distance(1) == distance(2)
end associate
end function is_45_degrees



elemental integer function maximum(self)
class(line_t), intent(in) :: self
maximum = maxval([self%from, self%till])
end function maximum



end module line_m
