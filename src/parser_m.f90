module parser_m
use strff, only: read_file_lines
use iso_varying_string, only: &
        varying_string, &
        assignment(=)
implicit none
private
public :: integer_parser, &
          get_number_of_lines

contains



! parse a file with one integer per line
function integer_parser(filename) result(integers)
character(*), intent(in) :: filename
integer, allocatable :: integers(:)

character(16)                     :: line
integer                           :: i

associate(lines => read_file_lines(filename))
    allocate(integers(size(lines)))
    do i=1,size(lines)
        line = lines(i)
        read(line, "(i16)") integers(i)
    end do
end associate
end function integer_parser



function get_number_of_lines(filename) result(number_of_lines)
character(*), intent(in) :: filename
integer :: number_of_lines

integer         :: fileunit
integer         :: ierr
character(1024) :: tmp

open(newunit=fileunit, file=filename)
number_of_lines = -1 ! to prevent eof counting as line
ierr = 0
do while(ierr == 0)
    number_of_lines = number_of_lines + 1
    read(fileunit, "(a)", iostat=ierr) tmp
end do
close(fileunit)
end function get_number_of_lines



end module parser_m
