module parser_input_m
use iso_fortran_env, only: int64
implicit none

type parser_input_t
    character(128) :: text
    integer        :: pos                = 1
    integer(int64) :: syntax_error_score = 0
    logical        :: end_of_input       = .false.
contains
    procedure :: is_end_of_input
    procedure :: peek
    procedure :: update_score
end type parser_input_t

interface parser_input_t
    module procedure :: constructor
end interface

contains



elemental function constructor(line) result(parser_input)
character(128), intent(in) :: line
type(parser_input_t) :: parser_input
parser_input%text = line
end function constructor



logical impure elemental function is_end_of_input(self)
class(parser_input_t), intent(inout) :: self
if(.not.self%end_of_input) then
    if(self%pos > 128) then
        self%end_of_input = .true.
    else
        self%end_of_input = self%text(self%pos:self%pos) == ' '
    end if
end if
is_end_of_input = self%end_of_input
end function is_end_of_input



character elemental function peek(self)
class(parser_input_t), intent(in) :: self
peek = self%text(self%pos:self%pos)
end function peek



impure elemental subroutine update_score(self, score)
class(parser_input_t), intent(inout) :: self
integer,               intent(in)    :: score
self%syntax_error_score = 5 * self%syntax_error_score + score
end subroutine update_score



end module parser_input_m
